#!/bin/bash
set -eu
set -x
kinit -R frigo@FEDORAPROJECT.ORG
git pull PROD HEAD
sleep 2
centpkg prep
git push origin HEAD
copr build-package --name kernel kernel
